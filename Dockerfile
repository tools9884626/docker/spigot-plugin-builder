ARG MAVEN_VERSION=3
ARG JAVA_VERSION=8

FROM maven:3-eclipse-temurin-8

WORKDIR /minecraft

# Change to the maven repository directory
COPY settings.xml /root/.m2/settings.xml
RUN mkdir -p .m2/repository

RUN mkdir lib
ADD https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar ./lib/BuildTools.jar

ARG SPIGOT_VERSION=1.15.1
RUN cd lib && java -Xmx1024M -jar BuildTools.jar --rev 1.15.1

# Copy .m2 folder to minecraft folder

# ENV MAVEN_CONFIG=.m2/repository

RUN ls -la .m2/repository
